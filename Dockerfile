FROM openjdk:11-jre-slim

COPY build/libs/*.jar /deployment/app.jar

CMD [ \
  "java", \
  "-jar", \
  "/deployment/app.jar", \
  "-XX:NativeMemoryTracking=summary", \
  "--spring.profiles.active=prod" \
]
