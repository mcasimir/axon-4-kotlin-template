# Axon 4 + Spring boot + mongodb + kotlin starter

## How to

### Create a new aggregate

``` kotlin
@Aggregate
class User() {
  @AggregateIdentifier
  private var userId: UserId? = null

  @CommandHandler
  constructor(cmd: CreateUserCommand) : this() {
    apply(UserCreatedEvent(cmd.userId))
  }

  @EventSourcingHandler
  fun on(evt: UserCreatedEvent) {
    userId = evt.userId
  }
}
```

### Create a new aggregate with compound entity id

``` kotlin
data class MyCompoundId(val tenantId: String, val userId: String) {
    override fun toString(): String {
        return "${this.tenantId}.${this.userId}"
    }
}

fun createUserId(tenantId: String) = BrandUserId(tenantId, UUID.randomUUID().toString())
```

### Test an aggregate

``` kotlin
import org.axonframework.test.aggregate.AggregateTestFixture
import org.junit.Before
import org.junit.Test

internal class UserTest {
    lateinit var fixture: AggregateTestFixture<User>

    @Before
    fun setUp() {
        fixture = AggregateTestFixture(User::class.java)
    }

    @Test
    fun testShouldAllowToCreateAUser() {
        val id = createUserId()

        fixture.given()
                .`when`(CreateUserCommand(id))
                .expectSuccessfulHandlerExecution()
                .expectEvents(UserCreatedEvent(id))
    }
}
```

### Send a command to an aggregate

``` kotlin
val command = CreateUserCommand("user-1)

commandGateway.send<CreateUserCommand>(command)

// or
commandGateway.sendAndWait<CreateUserCommand>(command)
```
