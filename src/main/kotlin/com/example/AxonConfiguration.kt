package com.example

import com.mongodb.MongoClient
import org.axonframework.extensions.mongo.DefaultMongoTemplate
import org.axonframework.extensions.mongo.eventsourcing.eventstore.MongoEventStorageEngine
import org.axonframework.serialization.json.JacksonSerializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.axonframework.eventsourcing.eventstore.EventStorageEngine
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore

@Configuration
@EnableMongoRepositories
class AxonConfiguration {

  @Bean
  fun eventStore(storageEngine: EventStorageEngine, configuration: AxonConfiguration): EmbeddedEventStore {

    return EmbeddedEventStore.builder()
      .storageEngine(storageEngine)
      .build()
  }

  @Bean
  fun storageEngine(client: MongoClient): EventStorageEngine {
    val jsonSerializer = JacksonSerializer.defaultSerializer()
    return MongoEventStorageEngine.builder()
      .eventSerializer(jsonSerializer)
      .snapshotSerializer(jsonSerializer)
      .mongoTemplate(DefaultMongoTemplate.builder().mongoDatabase(client).build())
      .build()
  }
}
